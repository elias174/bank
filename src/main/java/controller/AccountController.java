package controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Account;
import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.AccountService;




@Controller
public class AccountController {
	AccountRepository repository;
	AccountService service;
	@RequestMapping("/accounts")
	public void get_account_data(ModelMap model) {
		repository = new InMemoryAccountRepository();
		service = new AccountService(repository);
		service.generate_memory_users();	
		model.addAttribute("all_accounts", service.get_all_accounts());
	}

}
