package controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;
import service.AccountService;

@Controller
public class TransferController {
	AccountRepository repository;
	AccountService account_service;
	TransferService transfer_service;
	@RequestMapping("/transfer")
	public String input_data_transfer(ModelMap model){
		return "transfer";
	}
	@RequestMapping("/success_transfer")
	public String transfer_accepted(ModelMap model, @RequestParam String cuenta1, @RequestParam String cuenta2, @RequestParam String monto){
		repository = new InMemoryAccountRepository();
		account_service = new AccountService(repository);
		account_service.generate_memory_users();
		transfer_service = new TransferService(repository);
		double amonto = Double.parseDouble(monto);
		if(transfer_service.transfer(cuenta1, cuenta2, amonto )){
			return "success_transfer";
		}
		return "transfer";
	}
	
	
}
