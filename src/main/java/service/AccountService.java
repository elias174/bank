package service;

import java.util.List;

import repository.AccountRepository;
import domain.Account;

public class AccountService {
	AccountRepository repository;

	public AccountService(AccountRepository repository) {
		this.repository = repository;
	}
	public List<Account> get_all_accounts(){
		return repository.findAll();		
	}

	public void generate_memory_users(){
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		this.repository.save(a1);
		this.repository.save(a2);
	}


}
